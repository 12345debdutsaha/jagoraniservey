import React,{useEffect,useState} from 'react';
import './App.css';
import { BrowserRouter as Router, Route,Switch} from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css'
import Home from './pages/home'
import AddPlace from './pages/addPlace';
import AddDetails from './pages/addDetails';
import AllDetails from './pages/allDetails';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {ValidJwt} from './action/authAction'
//Authentication Context
import {AuthContext,authData} from './context/authclient'
import AllPlace from './pages/allPlaces';
import UpdatePlace from './client/components/updatePlaces';
const App=(props)=>{
  const [auth,setAuth]=useState(authData)
  const [loading,setLoading]=useState(true)
  useEffect(()=>{
    setLoading(true)
    getStatus()
    setLoading(false)
  },[])
  const getStatus=async()=>{
    const res=await ValidJwt()
    if(res.message==="Valid jwt" && res.admin){
      setAuth(
        {
          isAuthenticated:true,
          isAdmin:true,
          isSecAuthenticated:auth.isSecAuthenticated,
          user:{
            uid:res.user
          }
        }
      )
    }
    else if(res.message==="Valid jwt")
    {
        setAuth(
          {
            isAuthenticated:true,
            isAdmin:auth.isAdmin,
            isSecAuthenticated:auth.isSecAuthenticated,
            user:{
              uid:res.user
            }
          }
        )
    }
    else{
      toast.info(res.message)
    }
  }
  if(loading)
  {
    return(
      <div></div>
    );
  }else{
  return (
    <AuthContext.Provider value={[auth,setAuth]}>
    <ToastContainer/>
    <Router>
    <Switch>
      <Route exact path="/" component={Home}/>
      <Route exact path="/addPlace" component={AddPlace}/>
      <Route exact path="/addDetails/:id" component={AddDetails}/>
      <Route exact path="/allDetails/:id" component={AllDetails}/>
      <Route exact path="/allPlaces" component={AllPlace}/>
      <Route exact path="/updatePlace/:id" component={UpdatePlace}/>
    </Switch>
    </Router>
    </AuthContext.Provider>
  );
  }
}

export default App;