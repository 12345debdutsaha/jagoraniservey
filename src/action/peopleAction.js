import {url} from '../endpoint/urls'
import axios from 'axios';
import * as jsCookie from 'js-cookie'
import {AppFire} from '../firebase/index'
const authHeader=()=>{
    const signToken=jsCookie.get('signToken')
    return {headers:{ authorization: `Bearer ${signToken}`}}
}
export const addPeople=async(id,info)=>
{   
    let response=await axios.post(url+`/people/addPeople/${id}`,info,authHeader())
    .then(res=>{
        return res.data
    }).catch(err=>{
        return {
            status:401,
            message:"Error occured"
        }
    })
    if(response)
    {
        return response
    }
}

// export const firebaseFileUpload=async(file)=>{
//     return AppFire.storage().ref(`/uploads/${file.name}`).put(file)
//     .then(async result=>{
//         let url=result.ref.getDownloadURL().then(url=>{
//             return url
//         }).catch(err=>{
//             return undefined
//         })
//         return {
//             status:200,
//             url
//         }
//     }).catch(err=>{
//         return {
//             status:401,
//             message:err.message
//         }
//     })
// }

export const allDetails=async(id,option)=>{
    try{
        let response=await axios.get(url+`/people/fetch${option}/${id}`,authHeader())
        .then(result=>{
            if(result.data.length!==0)
            {
                return {
                    option:option,
                    data:result.data
                }
            }else{
                return {
                    status:401,
                    message:"No suitable data found"
                }
            }
        }).catch(err=>{
            return {
                status:401,
                message:"Some error occured"
            }
        })
        return response
    }catch(err)
    {
        return {
            status:401,
            message:"Unexpected error occured"
        }
    }
}

//delete particular one record
export const DeletePeople=async(id)=>{
    try{
        let response=await axios.delete(url+`/people/delete/oneperson/${id}`,authHeader())
        .then(result=>{
            if(result)
            {
                return result.data
            }else{
                return {
                    status:401,
                    message:"Something went wrong"
                }
            }
        }).catch(err=>{
            return {
                status:401,
                message:"Internal server error"
            }
        })
        return response
    }catch(err)
    {
        return {
            status:401,
            message:"Something went wrong"
        }
    }
}

//delete all place data
export const deleteAllPlaceData=async(pid)=>{
    try{
        const response=await axios.delete(url+`/people/delete/places/${pid}`,authHeader())
        .then(result=>{
            if(result.data)
            {
                return result.data
            }else{
                return {
                    status:401,
                    message:"Something went wrong"
                }
            }
        }).catch(err=>{
            return {
                status:401,
                message:"Internal server error"
            }
        })
        return response
    }catch(err)
    {
        return {
            status:401,
            message:"Something went wrong"
        }
    }
}


export const updateDone=async(id,done)=>{
    let donesample={
        done:done
    }
    let response=await axios.post(url+`/people/setDone/${id}`,donesample,authHeader()).then(response=>{
        return {
            status:response.status,
            success:response.data.success
        }
    }).catch(err=>{
        return {
            status:401,
            success:false
        }
    })
    console.log(response)
    return response
}