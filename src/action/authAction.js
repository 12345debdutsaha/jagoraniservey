import {url} from '../endpoint/urls'
import {AppFire} from '../firebase'
import axios from 'axios';
import * as jsCookie from 'js-cookie'
export const SignUpAction=async(data)=>{
    try{
    let newurl=url+"/api/auth/signup"
    let signToken=jsCookie.get("signToken")
    let res=await axios.post(newurl,data,{headers:{ authorization: `Bearer ${signToken}`}})
    .then(res=>res.data)
    .catch(err=>err)
    return res
    }catch(err)
    {
        return {message:"Error in connecting to server"}
    }
}

export const LoginAction=async(data)=>{
    try{
    var res=await AppFire.auth().signInWithEmailAndPassword(data.email,data.password)
    .then(res=>{
        return {
            message:"successfully logged in",
            user:res.user
        }
    })
    .catch((err)=>{
        return {
            message:err.message,
            user:undefined,

        }
    })
}catch(err){
    res={
        message:err.message,
        user:undefined
    }
}
    return res
}
export const ValidJwt=async()=>{
    let jwt=jsCookie.get('jwt')
    if(jwt!==undefined || jwt!==null)
    {
        let data={
            jwt:jwt
        }
        let nurl=url+"/api/auth/check/validJwt"
        var res=await axios.post(nurl,data)
        .then(res=>{
            return res.data
        }).catch(err=>{
            return err.message
        })
    }
    else{
        return {message:"You are not logged in you have to login"}
    }
    if(res)
    return res
    else
    return {message:"Some thing went wrong"}
}

export const Logout=async()=>{
    try{
    return await AppFire.auth().signOut().then((res)=>{
        jsCookie.remove('jwt')
        jsCookie.remove('refreshtoken')
        return {
            message:'succesfully logged out'
        }
    }).catch(err=>{
        return {
            message:err.message
        }
    })
}catch(err){
    return {
        message:err.message
    }
}
}

export const SecurityCheck=async(data)=>{
    try{
        let newurl=url+"/api/auth/check/securityKey"
        let val={
            secureKey:data
        }
        return await axios.post(newurl,val)
        .then((res)=>{
            return res.data
        }).catch(err=>{
            return err
        })
    }catch(err){
        return {message:err.message}
    }
}

//get the sitekey for recaptcha
export const getSiteKey=async()=>{
    try{
    let newurl=url+"/api/auth/getrecaptcha"
    let signToken=jsCookie.get("signToken")
    var res=await axios.get(newurl,{headers:{authorization:`Bearer ${signToken}`}})
    .then(response=>{
        return response.data
    }).catch(err=>{
        return {message:"Auth failed"}
    })
    }catch(err)
    {
        res={message:"Internal error"}
    }
    return res
}