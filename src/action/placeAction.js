import {url} from '../endpoint/urls'
import axios from 'axios';
import * as jsCookie from 'js-cookie'
const authHeader=()=>{
    const signToken=jsCookie.get('signToken')
    return {headers:{ authorization: `Bearer ${signToken}`}}
}
export const AddPlaceAction=async(data)=>{
    try{
        let newurl=url+'/place/add'

        let res=await axios.post(newurl,data,authHeader()).then(response=>{
            return response.data
        }).catch(err=>{
            return {status:401,message:err.message}
        })
        if(res)
        {
            return res
        }else{
            return {status:401,message:"Network Error"}
        }
    }catch(err)
    {
        return {status:401,message:"Network Error"}
    }
}

export const AllPlacesAction=async()=>{
    try{
        let newurl=url+'/place/allData'
        let res= await axios.get(newurl,authHeader()).then(response=>{
            return response.data
        }).catch(err=>{
            return {status:401,message:err.message}
        })
        if(res===undefined && res===null)
        {
            return {status:401,message:"Error occured"}
        }else{
            return res
        }
    }catch(err)
    {
        return {status:401,message:"Network error"}
    }
}
//Deleting the particular place
export const DeleteOneAction=async(id)=>{
    try{
        let newurl=url+`/place/delete/${id}`
        return await axios.delete(newurl,authHeader()).then(result=>{
            return result.data
        }).catch(err=>{
            return {status:401,message:err.message}
        })
    }catch(err)
    {
        return {status:401,message:"Network error"}
    }
}

//get All the data of particular place's id
export const GetDataOne=async(id)=>{
    try{
    let newurl=url+`/place/onedata/${id}`
    return await axios.get(newurl,authHeader()).then(result=>{
        return result.data
    }).catch(err=>{
        return {status:401,message:"Error occured"}
    })
}catch(err)
{
    return {status:401,message:"Network Error"}
}
}

//Update a place with data
export const UpdateDataAction=async(data,id)=>
{
    try{
    let newurl=url+`/place/update/${id}`
    return await axios.post(newurl,data,authHeader()).then(result=>{
        return result.data
    }).catch(err=>{
        return {status:401,message:"Error occured"+err.message}
    })
}catch(err){
    return {status:401,message:"NetWork error"}
}
}