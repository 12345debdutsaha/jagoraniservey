import firebase from 'firebase'

// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyC1Dh4MkoljBcX4KV9ERrVBoykmQ6R5FsI",
    authDomain: "sevey-2a924.firebaseapp.com",
    databaseURL: "https://sevey-2a924.firebaseio.com",
    projectId: "sevey-2a924",
    storageBucket: "sevey-2a924.appspot.com",
    messagingSenderId: "745158397057",
    appId: "1:745158397057:web:59787233c43fbbba"
  };

// Initialize Firebase
const fire=firebase.initializeApp(firebaseConfig)

export const AppFire=fire
