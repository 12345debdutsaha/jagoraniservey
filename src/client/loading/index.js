import React from 'react'
import '../../css/componentstyle/loading.css'

const Loading=()=>{
    return (
    <div className="container-loading">
    <div className="loading">
    <span style={{color:"#212121"}}>Loading...</span>
  </div>
  </div>
  )
}
export default Loading