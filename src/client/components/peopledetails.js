import React,{useState, useEffect} from 'react'
import '../../css/componentstyle/peopledetails.css'
import {Button} from 'reactstrap'
import {DeletePeople,updateDone} from '../../action/peopleAction'
import { toast } from 'react-toastify';
const PeopleDetails=(props)=>{
    const [done,setDone]=useState(false)
    const handleDelete=async()=>{
      let deleteOne=window.confirm("Are you sure want to delete?")
      if(deleteOne)
      {
        let response=await DeletePeople(props.id)
        if(response.status===200)
        {
          props.setDelId(props.id)
          toast.success("Successfully deleted the user")
        }else{
          toast.error(response.message)
        }
      }
    }
    useEffect(()=>{
      setDone(props.done)
    },[])
    const handleDone=async()=>{
      let response=await updateDone(props.id,true)
      if(response.success)
      {
        setDone(true)
      }else{
        toast.error("Server error occured")
      }
    }

    const handleUndone=async()=>
    {
      let response=await updateDone(props.id,false)
      if(response.success)
      {
        setDone(false)
      }else{
        toast.error("Server error")
      }
    }
    return(
           <div className="profile-card-4 z-depth-3">
            <div className="card">
              <div className="card-body text-center bg-primary rounded-top">
               <div className="user-box">
                <img src={props.photourl} alt="user avatar"/>
              </div>
             </div>
              <div className="card-body">
                <ul className="list-group shadow-none">
                <li className="list-group-item">
                  <div className="list-details">
                    <span>{props.name}</span>
                    <small>Name of the candidate</small>
                  </div>
                </li>
                <li className="list-group-item">
                  <div className="list-details">
                    <span>{props.option} : {props.size}</span>
                    <small>Catagory of cloths</small>
                  </div>
                </li>
                <li className="list-group-item">
                  <div className="list-details">
                    <Button color="warning" onClick={handleDelete}>Delete</Button>
                  </div>
                </li>
                <li className="list-group-item">
                  {done?<div className="list-details" style={{flex:1,flexDirection:'row'}}>
                    <i className="fa fa-check" style={{color:"green"}}></i>
                    <p style={{color:"green"}}>Done</p>
                    <Button color="info" onClick={handleUndone}>Undone</Button>
                  </div>:
                  <div className="list-details">
                    <Button color="success" onClick={handleDone}>Set Done</Button>
                  </div>
                  }
                </li>
                </ul>
               </div>
             </div>
           </div>
    );
}
export default PeopleDetails