import React from 'react'
import '../../css/navigation.css'
const NavigationTab=(props)=>{
    return(
        <div className={`navigation-container ${props.className}`} onClick={()=>{props.onClick()}}>
            <div className="navigation-text">
                <h3>{props.name}</h3>
                <h5>Lets sign up for access the hidden features</h5>
            </div>
        </div>
    );
}

export default NavigationTab;