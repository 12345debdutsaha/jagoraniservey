import React,{useState} from 'react'
import {Redirect,Link} from 'react-router-dom'
import {Card,CardBody,CardHeader,CardTitle,CardText,Button} from 'reactstrap'
import '../../css/componentstyle/place.css'
import {DeleteOneAction} from '../../action/placeAction'
import {toast} from 'react-toastify'
const PlaceComponent=(props)=>{
    const [redirect,setRedirect]=useState(false)
    const handleDelete=async()=>{
      const res=await DeleteOneAction(props.id)
      if(res)
      {
        if(res.status===200)
        {
          props.setId(props.id)
          toast.success(res.message)
        }else{
          toast.error(res.message)
        }
      }else
      {
        toast.error("Network error")
      }
    }
    if(redirect)
    {
      return <Redirect to={`/allPlaces`}/>
    }
    else{
    return <div>
    <Card className="portfolio-card">
      <CardHeader className="portfolio-card-header" style={{fontSize:25}}>Name: {props.name}</CardHeader>
        <CardBody>
          <p className="portfolio-card-city">Adress: {props.addressline}</p>
            <CardTitle className="portfolio-card-title">{props.landmark}</CardTitle>
              <CardText className="portfolio-card-text">{props.nearLocation}</CardText>
            <Button color="outline-success"  
            className="button-default" 
            size='small'
            onClick={handleDelete}
            >Delete</Button>
            <Link to={`/updatePlace/${props.id}`}><Button 
            color="outline-warning" className="button-default" 
            size='small'>Update</Button></Link>
             <Link to={`/addDetails/${props.id}`}><Button 
            color="outline-danger" className="button-default" 
            size='small'>Add People</Button></Link>
            <Link to={`/allDetails/${props.id}`}><Button 
            color="outline-danger" className="button-default" 
            size='small'>All People</Button></Link>
          </CardBody>
      </Card>
    </div>
    }
}
export default PlaceComponent;