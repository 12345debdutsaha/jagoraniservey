import React,{useState,useEffect} from 'react'
import NavbarShared from '../shared/navigation';
import {Input,Row,Col,Label,Button} from 'reactstrap'
import '../../css/addPlace.css'
import {GetDataOne,UpdateDataAction} from '../../action/placeAction'
import { toast } from 'react-toastify';
const UpdatePlace=(props)=>{
    useEffect(()=>{
        getOnePlaceData()
    },[])
    const [name,setName]=useState('')
    const [landmark,setLandMark]=useState('')
    const [nearLocation,setNearLocation]=useState('')
    const [addressline,setAddressLine]=useState('')

    //get one place data specified by id
    const handleSubmit=async()=>
    {
        if(name && landmark && nearLocation && addressline)
        {
            var data={
                name,
                landmark,
                nearLocation,
                addressline
            }
            let res=await UpdateDataAction(data,props.match.params.id)
            if(res)
            {
                toast.success(res.message)
            }else{
                toast.error("Network error")
            }
        }else{
            toast.error("All feilds are required please give all the info")
        }
    }
    const getOnePlaceData=async()=>{
        try{
        let res=await GetDataOne(props.match.params.id)
        if(res)
        {
            setName(res.data.name)
            setLandMark(res.data.landmark)
            setAddressLine(res.data.addressline)
            setNearLocation(res.data.nearLocation)
        }
    }catch(err)
    {
        toast.error(err.message)
    }
    }

    return (
        <React.Fragment>
            <div>
            <NavbarShared/>
            </div>
            <div>
            <Row>
            <Col md="6">
                <div className="input-container">
                    <Label>Name</Label>
                    <Input 
                    onChange={(e)=>{setName(e.target.value)}} 
                    value={name}/>
                </div>
                <div className="input-container">
                    <Label>Landmark</Label>
                    <Input 
                    onChange={(e)=>{setLandMark(e.target.value)}} 
                    value={landmark}/>
                </div>
                <div className="input-container">
                    <Label>NearLocation</Label>
                    <Input 
                    onChange={(e)=>{setNearLocation(e.target.value)}} 
                    value={nearLocation}/>
                </div>
                <div className="input-container">
                    <Label>AddressLine</Label>
                    <Input type="textarea" 
                    onChange={(e)=>{setAddressLine(e.target.value)}} 
                    value={addressline}/>
                </div>
                <div className="input-container">
                    <Button color="warning" 
                    className="submit-style" 
                    size="small"
                    onClick={handleSubmit}
                    >Submit</Button>
                </div>
            </Col>
        </Row>
            </div>
        </React.Fragment>
    );
}
export default UpdatePlace;