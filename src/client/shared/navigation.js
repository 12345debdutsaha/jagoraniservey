import React,{useState,useContext} from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  Nav,
  NavItem,
} from 'reactstrap';
import {toast} from 'react-toastify'
import Logo from '../../svgs/bars-solid.svg'
import {Link} from 'react-router-dom'
import '../../css/componentstyle/navbar.css'
import {Logout} from '../../action/authAction'
//AuthContext--->
import {AuthContext} from '../../context/authclient'
import {withRouter,Redirect} from 'react-router-dom'
const NavbarShared=(props)=>{
  const [authContext,setAuthContext]=useContext(AuthContext)
  const [isOpen,setisOpen]=useState(false)
  const [push,setPush]=useState(false)
  const toggle=()=> {
    setisOpen(!isOpen)
  }
  const handleLogout=async()=>{
    let res=await Logout()
    if(res.message==="succesfully logged out")
    {
      setAuthContext({
        isAuthenticated:false,
        isAdmin:authContext.isAdmin,
        isSecAuthenticated:authContext.isSecAuthenticated,
        user:{
          uid:authContext.user.uid
        }
      })
      toast.success(res.message)
    }else{
      toast.error(res.message)
    }
  }
  if(push)
  {
    return <Redirect to="/addPlace"/>
  }
  return (
    <div>
      <Navbar expand="md" className="navbar-bgc">
        <Link to="/" style={{textDecoration:'none',fontWeight:'bold',color:'white'}}>Jagorani Survey</Link>
        <NavbarToggler onClick={toggle}  style={{backgroundColor:'white'}}>
            <img src={Logo} style={{height:20,width:20,color:"#212121"}}></img>
        </NavbarToggler>
        <Collapse isOpen={isOpen} navbar>
          <Nav className="ml-auto" navbar>
            {authContext.isAuthenticated && <NavItem className="options-container">
             <Link to="/addPlace" style={{textDecoration:'none',fontWeight:'bold',color:'white'}}>Add Places</Link>
            </NavItem>}  
             {authContext.isAuthenticated && <NavItem className="options-container">
             <Link to="/allPlaces" style={{textDecoration:'none',fontWeight:'bold',color:'white'}}>All Places</Link>
            </NavItem>}
             {authContext.isAuthenticated && <NavItem className="options-container">
              <p onClick={handleLogout} style={{textDecoration:'none',fontWeight:'bold',cursor:'pointer'}}>Log Out</p>
             </NavItem>}
          </Nav>
        </Collapse>
      </Navbar>
    </div>
  );
}
export default withRouter(NavbarShared);