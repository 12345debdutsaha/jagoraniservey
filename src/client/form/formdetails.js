import React,{useState} from 'react'
import {Form,Input,Row,Col,Label,Button} from 'reactstrap'
import Loading from '../loading';
import '../../css/componentstyle/formdetails.css';
import {addPeople} from '../../action/peopleAction'
import { toast } from 'react-toastify';
import {AppFire} from '../../firebase/index'
const FormDetails=(props)=>{
    const [boy,setBoy]=useState(0)
    const [girl,setGirl]=useState(0)
    const [kurti,setKurti]=useState("")
    const [sharee,setSharee]=useState("young")
    const [name,setName]=useState("")
    const [loading,setLoading]=useState(false)
    const [boydisabled,setboyDisabled]=useState(false)
    const [girldisabled,setgirlDisabled]=useState(false)
    const [kurtidisabled,setkurtiDisabled]=useState(false)
    const [shareedisabled,setshareeDisabled]=useState(false)
    const [submitDisable,setSubmitDisable]=useState(false)
    const [url,setUrl]=useState("")

    const getInfo=()=>{
        let info={
            boy:boydisabled?0:boy,
            girl:girldisabled?0:girl,
            kurti:kurtidisabled?0:kurti,
            sharee:shareedisabled?"":sharee,
            name,
            photourl:url
        }
        //console.log(info)
        return info
    }


    const handleSubmit=async()=>{
        setLoading(true)
        let info=getInfo()
        if(info.photourl.length!==0)
        {
            let resownserver=await addPeople(props.id,info)
            if(resownserver.status===200)
            {
                toast.success(resownserver.message)
                handleRefresh()
                setName("")
            }else{
                toast.error(resownserver.message)
            }
        }else{
            toast.error("Not yet uploaded the file")
            setLoading(false)
        }    
        setLoading(false)
    }

    const handleFileSubmit=async(e)=>
    {
        setLoading(true)
        if(e.target.files[0])
        {
        let file=e.target.files[0];
        let filename=file.name;
        var storageRef=AppFire.storage().ref(`/images/${filename}`)
        storageRef.put(file).then((snap)=>{
            storageRef.getDownloadURL().then((url)=>{
                setUrl(url)
            setLoading(false)
        }).catch(err=>
            {
                toast.error("Error occured in uploading the file")
                setLoading(false)
            })
    })
}
}


    const handleRefresh=()=>{
        setkurtiDisabled(false)
        setgirlDisabled(false)
        setshareeDisabled(false)
        setboyDisabled(false)
        setGirl(10)
        setBoy(10)
        setKurti("")
        setSharee("young")
    }



    if(loading)
    {
        return <Loading/>
    }
    return(
        <React.Fragment>
            <Row>
                <Col md="6">
                    <Form>
                        <div className="input-container">
                        <Label for="exampleSelect" className="label-style">Name</Label>
                        <Input placeholder="name" value={name} onChange={(e)=>{
                            setName(e.target.value)
                        }} className="input-style"/>
                        </div>
                        <div className="input-container">
                        <Label for="exampleSelect" className="label-style">Boy's Category</Label>
                        <Input type="select" value={boy} disabled={boydisabled} onChange={(e)=>{
                            setkurtiDisabled(true)
                            setgirlDisabled(true)
                            setshareeDisabled(true)
                            setBoy(e.target.value)
                        }} name="select" id="exampleSelect" 
                        className="input-style">
                            <option>10</option>
                            <option>12</option>
                            <option>14</option>
                            <option>16</option>
                            <option>18</option>
                            <option>20</option>
                            <option>22</option>
                            <option>24</option>
                            <option>26</option>
                            <option>28</option>
                            <option>30</option>
                        </Input>
                        </div>
                        <div className="input-container">
                        <Label for="exampleSelect" className="label-style">Girl's Category</Label>
                        <Input type="select" value={girl} disabled={girldisabled} onChange={(e)=>{
                            setGirl(e.target.value)
                            setkurtiDisabled(true)
                            setboyDisabled(true)
                            setshareeDisabled(true)
                        }} name="select" id="exampleSelect" className="input-style">
                            <option>10</option>
                            <option>12</option>
                            <option>14</option>
                            <option>16</option>
                            <option>18</option>
                            <option>20</option>
                            <option>22</option>
                            <option>24</option>
                            <option>26</option>
                            <option>28</option>
                            <option>30</option>
                        </Input>
                        </div>
                        <div className="input-container">
                        <Label for="exampleSelect" className="label-style">Kurti</Label>
                        <Input type="text" value={kurti} disabled={kurtidisabled} onChange={(e)=>{
                            setgirlDisabled(true)
                            setboyDisabled(true)
                            setshareeDisabled(true)
                            setKurti(e.target.value)
                        }} name="select" id="exampleSelect" className="input-style">
                        </Input>
                        </div>
                        <div className="input-container">
                        <Label for="exampleSelect" className="label-style">Sharee's Catagory</Label>
                        <Input type="select" value={sharee} disabled={shareedisabled} onChange={(e)=>{
                            setkurtiDisabled(true)
                            setboyDisabled(true)
                            setgirlDisabled(true)
                            setSharee(e.target.value)
                        }} name="select" id="exampleSelect" className="input-style">
                            <option>young</option>
                            <option>middle age</option>
                            <option>old age</option>
                        </Input>
                        </div>
                        <div className="file-container">
                        <div className="upload-btn-wrapper">
                            <button className="btn">Upload a file</button>
                            <input type="file" name="myfile" onChange={handleFileSubmit}/>
                        </div>
                        </div>
                        <div className="file-container">
                            <Button onClick={handleSubmit} disabled={submitDisable} color="warning">Submit</Button>
                            <Button onClick={handleRefresh} color="success">Refresh</Button>
                        </div>
                    </Form>
                </Col>
            </Row>
        </React.Fragment>
    );
}

export default FormDetails;