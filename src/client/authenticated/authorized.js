import React from 'react'
import '../../css/componentstyle/auth.css'
import NavbarShared from '../shared/navigation'
const Authorized=()=>{
    return(
        <React.Fragment>
        <NavbarShared/>
        <div className="auth-container">
            <p className="auth-text">You are not authorized to access data</p>
        </div>
        </React.Fragment>
    );
}
export default Authorized