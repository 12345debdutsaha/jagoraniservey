import React from 'react'
import '../../css/componentstyle/auth.css'
import NavbarShared from '../shared/navigation'
const Authenticated=()=>{
    return(
        <React.Fragment>
        <NavbarShared/>
        <div className="auth-container">
            <p className="auth-text">Welcome to JAGORANI</p>
        </div>
        </React.Fragment>
    );
}
export default Authenticated