import React,{useState,useContext} from 'react'
import {Modal,ModalBody,Input,Label,Button} from 'reactstrap'
import '../../css/componentstyle/securitycheck.css'
import * as authAction from '../../action/authAction'
import {toast} from 'react-toastify'
import {AuthContext} from '../../context/authclient'

import jsCookie from 'js-cookie'
const SecurityCheck=(props)=>{
    const [Open,setOpen]=useState(true)
    const [data,setData]=useState('')
    const [authContext,setAuthContext]=useContext(AuthContext)
    const Validate=async()=>{
        let res=await authAction.SecurityCheck(data)
        if(res.message==="successfull")
        {
            jsCookie.set('signToken',`${res.token}`)
            setAuthContext({
                    isAuthenticated:authContext.isAuthenticated,
                    isAdmin:authContext.isAdmin,
                    isSecAuthenticated:true,
                    user:{
                        uid:authContext.user.uid
                    }
                })
            toast.success("Successfully security checking has done")
        }else{
            toast.error(res.message)
        }
    }
    return(
        <div className="secure-modal">
            <Modal isOpen={Open}>
                <ModalBody>
                    <div>
                        <Label>Enter Security Key</Label>
                        <Input onChange={(e)=>{setData(e.target.value)}}/>
                        <Button color="primary" onClick={Validate} 
                        style={{marginTop:10,fontSize:15}}>
                        Enter Code</Button>
                    </div>
                </ModalBody>
            </Modal>
        </div>
    );
}  

export default SecurityCheck