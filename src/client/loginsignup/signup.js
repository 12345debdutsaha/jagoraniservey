import React from 'react'
import CommonSign from './commonsign'
const SignUpModalCom=(props)=>{
    return(
        <CommonSign {...props} heading="Sign Up"/>
    );
}
export default SignUpModalCom