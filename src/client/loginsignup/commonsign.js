import React,{useState,useContext,useEffect} from 'react'
import {Modal,ModalHeader,ModalBody,Alert} from 'reactstrap'
import '../../css/componentstyle/modal.css'
import {withRouter} from 'react-router-dom'
import {SignUpAction,LoginAction,getSiteKey} from '../../action/authAction'
import {toast} from 'react-toastify'
import {AuthContext} from '../../context/authclient'

//recaptcha reqirement
var Recaptcha = require('react-recaptcha');

const jsCookie=require('js-cookie')
let toastOption={
    position: "top-right",
    autoClose: 5000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true
}
const CommonSign=(props)=>{

    const [email,setEmail]=useState('')
    const [password,setPassword]=useState('')
    const [phonenumber,setPhone]=useState('')
    const [conPassword,setconPassword]=useState('')
    const [errormessage,setError]=useState([])
    const [authContext,setAuthContext]=useContext(AuthContext)
    const [sitekey,setSiteKey]=useState('')
    const [verified,setVerified]=useState(false)
    useEffect(()=>{
        getcaptcha()
    },[])
    const getcaptcha=async()=>{
        try{
        const res=await getSiteKey()
        if(res.message==="successfull")
        {
            setSiteKey(res.captcha)
        }else{
            toast.warn(res.message)
        }
    }catch(err)
    {
        toast.error("Error occured")
    }
    }
    const validation=()=>{
        var err=[]
        if(props.heading==='Sign Up')
        {
        if(!email && !password && !phonenumber && !conPassword)
        {
            err.push("All feilds are required")
        }else{
            if(password !== conPassword)
            {
                err.push("The password should be same for both case")
            }
            if(password.length<6)
            {
                err.push("The password should contain atleast 6 charecter")
            }
        }
    }else{
        if(!email && !password)
        {
            err.push("All Feilds are required")
        }
    }
    if(err.length!==0)
    {
        setError(err)
        return false
    }
    else{
        return true
    }
    }
    const handleSubmit=async()=>{
        if(props.heading==='Sign Up')
        {
        var bool=validation()
        if(bool && verified)
        {
            var data={
                email,
                password,
                phonenumber,
                conpassword:conPassword
            }
            try{
            const res= await SignUpAction(data)
            if(res.message=="successfully created user")
            {
                toast.success(res.message,toastOption)
            }
            else{
                toast.warn(res.message,toastOption)
            }
        }catch(err){
            toast.error(err.message,toastOption)
        }
        }else{
            toast.error("You haven't verify the captcha")
        }
    }
    }
    const verifyCallback=(response)=>
    {
        if(response)
        {
            setVerified(true)
        }else{
            setVerified(false)
        }
    }
    return(
        <React.Fragment>
        <Modal isOpen={props.modalOpen} className="modal-container">
        <div className="container-div">
            <ModalHeader className="heading-modal">
                <p>{props.heading}
                <i className="fa fa-close" style={{marginLeft:50,cursor:'pointer'}} onClick={()=>{props.onClick()}}></i>
                </p>
            </ModalHeader>
            {!props.confirmHide && <ModalBody>
            <div className="wrap-input100 validate-input m-b-20" data-validate="Enter username or email">
            <input className="input100" onChange={(e)=>{
                setError([])
                setPhone(e.target.value)}} autoCorrect="off" autoComplete="on" spellCheck={false} type="text" name="phonenumber" placeholder="Phonenumber"/>
            <span className="focus-input100"></span>            
            </div>
            </ModalBody>}
            <br/>
            <p>Please give +91.......... formated phonenumber</p>
            <br/>
            <ModalBody>
            <div className="wrap-input100 validate-input m-b-20" data-validate="Enter username or email">
            <input className="input100" onChange={(e)=>{
                setError([])                
                setEmail(e.target.value)}} autoCorrect="off" autoComplete="on" spellCheck={false} type="email" name="username" placeholder="username or email"/>
            <span className="focus-input100"></span>            
            </div>
            </ModalBody>
            <ModalBody style={{marginTop:20}}>
            <div className="wrap-input100 validate-input m-b-20" data-validate="Enter username or email">
            <input className="input100" onChange={(e)=>{
                setError([])                
                setPassword(e.target.value)}} type="password" name="username" placeholder="Enter password"/>
            <span className="focus-input100"></span>
            </div>
            </ModalBody>
            {!props.confirmHide && <ModalBody style={{marginTop:20}}>
            <div className="wrap-input100 validate-input m-b-20" data-validate="Enter username or email">
            <input className="input100" onChange={(e)=>{
                setError([])                
                setconPassword(e.target.value)}} type="password" name="username" placeholder="Confirm password"/>
            <span className="focus-input100"></span>
            </div>
            </ModalBody>}
            {(!props.confirmHide && sitekey==='')?
            <div className="loader"></div>:
            <div className="recaptcha">
            <Recaptcha
                sitekey={sitekey}
                verifyCallback={verifyCallback}
              />
            </div>
            }
            <ModalBody>
            <div className="container-login100-form-btn">
            <button className="login100-form-btn" onClick={handleSubmit}>
                {props.heading}
            </button>
            </div>
            </ModalBody>
            <div>
            {errormessage.map((item,i)=>{
                return (
                <Alert color="danger" key={i}>
                    {item}
              </Alert>)
            })}
            </div>
        </div>
        </Modal>
        </React.Fragment>
    );
}
export default withRouter(CommonSign);