import React from 'react'
import CommonLog from './commonlog';
const LoginModalCom=(props)=>{
    return(
        <CommonLog {...props} heading="Log in" confirmHide={true}/>
    );
}
export default LoginModalCom