import React,{useContext} from 'react'
import NavbarShared from '../client/shared/navigation';
import FormDetails from '../client/form/formdetails';
import '../css/componentstyle/formdetails.css';
import {AuthContext} from '../context/authclient'
import Authorized from '../client/authenticated/authorized';
import SecurityCheck from '../client/securitycheck';
const AddDetails=(props)=>{
    const [authContext]=useContext(AuthContext)
    const { match: { params } } = props;
    if(!authContext.isAuthenticated)
    {
        return <Authorized/>
    }
    if(!authContext.isSecAuthenticated)
    {
        return <SecurityCheck/>
    }
    return(
        <React.Fragment>
            <div>
                <NavbarShared/>
            </div>
            <div className="formdetails-container">
                <FormDetails id={params.id}/>
            </div>
        </React.Fragment>
    );
}

export default AddDetails;