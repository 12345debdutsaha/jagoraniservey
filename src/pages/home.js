import React,{useState,useContext} from 'react'
import {Col,Row,Container,Button} from 'reactstrap'
import '../css/homestyle.css'
import NavigationTab from '../client/components/navigationTab';
import SignUpModalCom from '../client/loginsignup/signup';
import LoginModalCom from '../client/loginsignup/login';
import NavbarShared from '../client/shared/navigation';
import Authenticated from '../client/authenticated'
//import AuthContext api for authentication
import {AuthContext} from '../context/authclient'
import SecurityCheck from '../client/securitycheck';
const Home = (props)=>{
    
    //all states are created
    const [modal,setModal]=useState(true)
    const [loginModal,setLoginModal]=useState(false)
    const [SignUpModal,setSignUpModal]=useState(false)
    //use context api which is global in the entire application
    const [authContext,setAuthContext]=useContext(AuthContext)
    
    //onclick method for signup modal
    const onClickSignUp=()=>{
        setSignUpModal(!SignUpModal)
    }
    //onclick method for login modal
    const onClickLogin=()=>{
        setLoginModal(!loginModal)
    }
    if(authContext.isAuthenticated && authContext.isSecAuthenticated)
    {
        return <Authenticated/>
    }
    if(!authContext.isSecAuthenticated)
    {
        return <SecurityCheck/>
    }
    return(
        <React.Fragment>
        {SignUpModal && <SignUpModalCom modalOpen={SignUpModal} onClick={onClickSignUp}/>}
        {loginModal && <LoginModalCom modalOpen={loginModal} onClick={onClickLogin}/>}
        <NavbarShared/>
        <Container>
            <Row>
                <Col className="navigation-tab-container">
                    <NavigationTab name="SignUp" className="tap1" onClick={onClickSignUp}/>
                    <NavigationTab name="Login" className="tap2" onClick={onClickLogin}/>
                </Col>
            </Row>
        </Container>
        </React.Fragment>
    );
}

export default Home