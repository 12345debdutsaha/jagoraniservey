import React,{useContext,useState} from 'react'
import '../css/addPlace.css';
import NavbarShared from '../client/shared/navigation';
import {AuthContext} from '../context/authclient'
import Authorized from '../client/authenticated/authorized';
import SecurityCheck from '../client/securitycheck';
import {Input,Row,Col,Label,Button} from 'reactstrap'
//Action for database connection from client
import {AddPlaceAction} from '../action/placeAction'
import {toast} from 'react-toastify'
const ContainerPlace=React.memo((props)=>{


    const [authContext]=useContext(AuthContext)
    const [name,setName]=useState('')
    const [landmark,setLandMark]=useState('')
    const [nearLocation,setNearLocation]=useState('')
    const [addressline,setAddressLine]=useState('')
    
    
    const handlesubmit=async()=>{
        if(name && landmark && nearLocation && addressline)
        {
            const data={
                name,
                landmark,
                nearLocation,
                addressline
            }
            const res=await AddPlaceAction(data)
            if(res)
            {
                if(res.status===200)
                {
                    toast.success(res.message)
                }else{
                    toast.error(res.message)
                }
                setName('')
                setAddressLine('')
                setLandMark('')
                setNearLocation('')
            }else{
                toast.error("Network Error")
            }
        }else{
            toast.warn("All feilds are required")
        }
    }
    
    
    
    if(!authContext.isAuthenticated)
    {
        return <Authorized/>
    }
    if(!authContext.isSecAuthenticated)
    {
        return <SecurityCheck/>
    }
    return(
        <React.Fragment>
        <div>
            <NavbarShared/>
        </div>
        <Row>
            <Col md="6">
                <div className="input-container">
                    <Label>Name</Label>
                    <Input 
                    onChange={(e)=>{setName(e.target.value)}} 
                    value={name}/>
                </div>
                <div className="input-container">
                    <Label>Landmark</Label>
                    <Input 
                    onChange={(e)=>{setLandMark(e.target.value)}} 
                    value={landmark}/>
                </div>
                <div className="input-container">
                    <Label>NearLocation</Label>
                    <Input 
                    onChange={(e)=>{setNearLocation(e.target.value)}} 
                    value={nearLocation}/>
                </div>
                <div className="input-container">
                    <Label>AddressLine</Label>
                    <Input type="textarea" 
                    onChange={(e)=>{setAddressLine(e.target.value)}} 
                    value={addressline}/>
                </div>
                <div className="input-container">
                    <Button color="warning" 
                    className="submit-style" 
                    size="small"
                    onClick={handlesubmit}
                    >Submit</Button>
                </div>
            </Col>
        </Row>
        </React.Fragment>
    )
})
const AddPlace=(props)=>{
    return(
        <ContainerPlace/>
    );
}
export default AddPlace