import React,{useState,useEffect,useContext} from 'react'
import {AllPlacesAction} from '../action/placeAction'
import {toast} from 'react-toastify'
import PlaceComponent from '../client/components/place'
import {AuthContext} from '../context/authclient'
import Authorized from '../client/authenticated/authorized';
import SecurityCheck from '../client/securitycheck';
import NavbarShared from '../client/shared/navigation';
import {Row,Col} from 'reactstrap'
const AllPlace=(props)=>{
    const [places,setPlaces]=useState([])
    const [authContext]=useContext(AuthContext)
    const [id,setId]=useState("")
    useEffect(()=>{
        getAllPlaces()
    },[id])
    const getAllPlaces=async()=>{
        try{
        let res=await AllPlacesAction()
        if(res)
        {
            if(res.status===200)
            {
                setPlaces(res.data)
            }else{
                toast.error(res.message)
            }
        }else{
            toast.error("Network error")
        }
    }catch(err)
    {
        toast.error(err.message)
    }
    }
    if(!authContext.isAuthenticated)
    {
        return <Authorized/>
    }
    if(!authContext.isSecAuthenticated)
    {
        return <SecurityCheck/>
    }
    return (
        <React.Fragment>
        <div>
            <NavbarShared/>
        </div>
        {(places.length>=0)?<div style={{padding:20}}>
            <Row>
            {places.map((item,i)=>{
                return <Col md="4" key={i}>
                <PlaceComponent 
                id={item._id}
                name={item.name} 
                landmark={item.landmark} 
                addressline={item.addressline}
                nearLocation={item.nearLocation}
                setId={setId}
                />
                </Col>
            })}
            </Row>
        </div>:<div><p style={{textAlign:'center',fontSize:20,fontWeight:'bold',fontStyle:'italic'}}>No data found</p></div>}
        </React.Fragment>
    );
}
export default AllPlace