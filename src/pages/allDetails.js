import React,{useEffect, useState,useContext} from 'react'
import {Input,Label,Row,Col,Button} from 'reactstrap'
import {allDetails,deleteAllPlaceData} from '../action/peopleAction'
import NavbarShared from '../client/shared/navigation';
import PeopleDetails from '../client/components/peopledetails';
import { toast } from 'react-toastify';
import Loading from '../client/loading/index'
import {AuthContext} from '../context/authclient'
import Authorized from '../client/authenticated/authorized';
import '../css/componentstyle/searchbar.css'
const optionsort=[10,12,14,16,18,20,22,24,26,28,30]
const AllDetails=(props)=>{
    const [authContext]=useContext(AuthContext)
    const [Data,setData]=useState([])
    const [option,setOption]=useState("Boys")
    const [loading,setLoading]=useState(false)
    const [delId,setDelId]=useState("")
    const [searchString,setSearch]=useState("")
    const [originaldata,setOriginal]=useState([])
    const [totalcount,setTotalCount]=useState(0)
    const {match:{params}}=props
    
    const handleSearch=()=>{
        setLoading(true)
        let duplicate=originaldata.filter((item,index)=>{
            if(item.name.toLowerCase().includes(searchString.toLowerCase()))
            {
                return true
            }
        })
        setTotalCount(duplicate.length)
        setData(duplicate)
        setLoading(false)
    }
    useEffect(()=>{
        handleData()
    },[option,delId])
    
    const handleDelete=async()=>{
        setLoading(true)
        const response=await deleteAllPlaceData(params.id)
        if(response.status===200)
        {
            setData([])
            toast.success(response.message)
        }else{
            toast.error(response.message)
        }
        setLoading(false)
    }
    
    const handleSort=(e)=>
    {
        try{
        let sample=e.target.value
        if(option==="Boys")
        {
            let duplicate=originaldata.filter((item,index)=>{
                if(item.boy===parseInt(sample))
                {
                    return true
                }
            })
            setTotalCount(duplicate.length)
            setData(duplicate)
        }
        if(option==="Girls")
        {
            let duplicate=originaldata.filter((item,index)=>{
                if(item.girl===parseInt(sample))
                {
                    return true
                }
            })
            setTotalCount(duplicate.length)
            setData(duplicate)
        }
        if(option==="Kurtis")
        {
            let temp=parseInt(sample)
            let duplicate=originaldata.filter((item,index)=>{
                if(item.kurti===temp)
                {
                    return true
                }
            })
            setTotalCount(duplicate.length)
            setData(duplicate)
        }
        if(option==="Sharees")
        {
            let duplicate=originaldata.filter((item,index)=>{
                if(item.sharee===sample)
                {
                    return true
                }
            })
            setTotalCount(duplicate.length)
            setData(duplicate)
        }
    }catch(err)
    {
        toast.error("Give a number not anyother thing")
    }
    }



    const handleData=async()=>{
        setLoading(true)
        let response=await allDetails(params.id,option)
        if(response.data.status===200)
        {
            setOption(response.option)
            setOriginal(response.data.data)
            setData(response.data.data)
            setTotalCount(response.data.data.length)
        }else{
            toast.error(response.message)
        }
        setLoading(false)
    }
    if(loading)
    {
        return <Loading/>
    }
    if(!authContext.isAuthenticated)
    {
        return <Authorized/>
    }
    return(
    <div>
        <NavbarShared/>
        <Row>
        <Col md="4">
        <Label for="exampleSelect" style={{padding:15,color:"purple",fontWeight:"bold"}}>Select a choise</Label>
          <Input type="select" name="select" value={option} id="exampleSelect" onChange={(e)=>{
              setOption(e.target.value)
          }}>
            <option>Boys</option>
            <option>Girls</option>
            <option>Kurtis</option>
            <option>Sharees</option>
          </Input>
          </Col>
          <Col md="4">
          <div style={{display:'flex',padding:30}}>
          <div className="searchbar">
          <input className="search_input" type="text" onBlur={(e)=>{
              setSearch(e.target.value)
          }} 
            placeholder="Search..."/>
          <a className="search_icon" onClick={()=>{
              handleSearch()
          }}><i className="fa fa-search"></i></a>
          </div>
          </div>
          {authContext.isAdmin && <div>
              <Button color="warning" style={{marginLeft:15,marginTop:10}} 
              onClick={handleDelete}
              >Delete All Place Data</Button>
          </div>}
          </Col>
          <Col md="4">
        <Label for="exampleSelect" style={{padding:15,color:"purple",fontWeight:"bold"}}>Sorting Selection</Label>
          {(option==="Boys" || option==="Girls")?<Input type="select" name="select" id="exampleSelect" onChange={handleSort}>
            {optionsort.map((item,index)=>{
                return (
                    <option key={index}>{item}</option>
                )
            })}
          </Input>:
          (option==="Sharees")?<Input type="select"  onChange={handleSort}>
            <option>young</option>
            <option>middle age</option>
            <option>old age</option>
          </Input>:<Input type="text" onChange={handleSort}>

          </Input>
          }
          </Col>
        </Row>
        <div style={{marginLeft:20}}>
            <Label><strong style={{color:"#212121",fontSize:30}}>Count: </strong>
            <p style={{color:"#212121",fontSize:25}}>{totalcount}</p></Label>
        </div>
        {(Data.length!==0)?<div>
            <Row>
            {Data.map((item,index)=>{
                let size;
                if(option==="Boys")
                {
                    size=item.boy
                }else if(option==="Girls")
                {
                    size=item.girl
                }else if(option==="Kurtis")
                {
                    size=item.kurti
                }else{
                    size=item.sharee
                }
                return (
                    <Col md="4" style={{margin:10}} key={index}>
                        <PeopleDetails 
                        id={item._id} 
                        name={item.name} 
                        photourl={item.photourl}
                        option={option}
                        size={size}  
                        setDelId={setDelId}  
                        done={item.done} 
                        />
                    </Col>
                );
            })}
            </Row></div>:
            <div style={{padding:20}}><p style={{textAlign:'center',fontSize:20,fontStyle:'italic'}}>
            No Data found</p></div>}
    </div>
    );
}
export default AllDetails