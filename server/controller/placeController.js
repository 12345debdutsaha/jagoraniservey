//requirement of the place schema
const Place=require('../models/Place')
//requirement of mongoose for creation of object id
const mongoose=require('mongoose')
//requirement of people schema to delete all the people coresponding the places
const People=require('../models/People')

exports.addPlace=(req,res,next)=>{
    try{
    Place.find({name:req.body.name},(err,result)=>{
        if(err)
        {
            res.send({
                status:401,
                message:"error occured",
                err:err.message
            })
        }else{
            if(result.length!==0)
            {
                res.send({
                    status:401,
                    message:"Entry name is already exist please do use another name"
                })
            }
            else{
                const place=new Place({
                    _id:mongoose.Types.ObjectId(),
                    name:req.body.name,
                    landmark:req.body.landmark,
                    nearLocation:req.body.nearLocation,
                    addressline:req.body.addressline
                })
                place.save().then(data=>{
                    if(data)
                    {
                        res.send({
                            status:200,
                            message:"Successfully created",
                            data
                        })
                    }
                }).catch(err=>{
                    res.send({
                        status:401,
                        message:"Error in saving the data",
                        err:err.message
                    })
                })
            }
        }
    })
}catch(err){
    res.send({
        status:401,
        message:"Internal server error"
    })
}
}

//updation of places
exports.updatePlace=(req,res,next)=>{
    try{
    const id = req.params.id
    const name=req.body.name
    const landmark=req.body.landmark
    const nearLocation=req.body.nearLocation
    const addressline=req.body.addressline
    if(landmark && nearLocation && addressline)
    {
    if(id)
    {
        Place.updateOne({_id:id},{$set:{name,landmark,nearLocation,addressline}}).then(result=>{
            res.send({
                status:200,
                message:"Successfully updated",
                response:result
            })
        }).catch(err=>{
            res.send({
                status:401,
                message:"Error occured",
                err:err.message
            })
        })
    }
    else{
        res.send({
            status:401,
            message:"Id is not specified"
        })
    }
}else{
    res.send({
        status:401,
        message:"Please give all credential"
    })
}
    }catch(err)
    {
        res.send({
            status:401,
            message:"Internal server error"
        })
    }
}

//Deleting a place from database
exports.deletePlace=(req,res,next)=>{
    try{
    const id=req.params.id
    if(id)
    {
        Place.deleteOne({_id:id}).then(result=>{
            if(result)
            {
                People.deleteMany({placeid:id}).then(presult=>{
                    if(presult)
                    {
                        res.json({
                            status:200,
                            message:"Successfully deleted"
                        })
                    }else{
                        res.json({
                            status:401,
                            message:"Something goes wrong"
                        })
                    }
                }).catch(err=>{
                    res.json({
                        status:401,
                        message:"Database error "+err.message
                    })
                })
            }else{
                res.send({
                    status:401,
                    message:"Error occured"
                })
            }
        }).catch(err=>{
            res.send({
                status:401,
                message:"Error occured while deleting from database",
                err:err.message
            })
        })
    }
    else{
        res.send({
            status:401,
            message:"Id is required in parameter"
        })
    }
}catch(err)
{
    res.send({
        status:401,
        message:"Internal server error"
    })
}
}


//deleting all the records at a time 
exports.deletePlaceAll=(req,res,next)=>{
    try{
    Place.deleteMany({}).then(result=>{
        if(result)
        {
            res.send({
                status:200,
                message:"Successfully delted all the values"
            })
        }else{
            res.send({
                status:401,
                message:"Error while deleting all the values"
            })
        }
    }).catch(err=>{
        res.send({
            status:401,
            message:"Error while deleting the data"+err.message
        })
    })
}catch(err)
{
    res.send({
        status:401,
        message:"Internal server error"
    })
}
}


//get all the places from databases
exports.getAllData=(req,res,next)=>{
    try{
        Place.find().then(doc=>{
            console.log(doc)
            if(doc.length!==0)
            {
                res.send({
                    status:200,
                    message:"Successfully fetched all data",
                    data:doc
                })
            }else{
                res.send({
                    status:200,
                    message:"successfully fetched all data",
                    data:[]
                })
            }
        }).catch(err=>{
            res.send({
                status:401,
                message:"Error occured while fetching the data"
            })
        })
    }catch(err)
    {
        res.send({
            status:401,
            message:"Internal server error"
        })
    }
}

//get a particular data
exports.getParticularData=(req,res,next)=>{
    try{
        const id=req.params.id
        Place.findOne({_id:id}).then(doc=>{
            if(doc)
            {
            res.send({
                status:200,
                message:"Successfully fetched the data",
                data:doc
            })
        }else{
            res.send({
                status:401,
                message:"Can't find anything"
            })
        }
        }).catch((err)=>{
            res.send({
                status:401,
                message:"Error in fecthing the particular data you have specified"
            })
        })
    }catch(err){
        res.send({
            status:401,
            message:"Internal server error"
        })
    }
}