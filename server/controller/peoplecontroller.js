const People=require('../models/People')
const mongoose=require('mongoose')
exports.addPeople=(req,res,next)=>{
    let name=req.body.name
    let photourl=req.body.photourl
    let boy=req.body.boy
    let girl=req.body.girl
    let kurti=req.body.kurti
    let sharee=req.body.sharee
    try{
        boy=parseInt(boy)
        girl=parseInt(girl)
        kurti=parseInt(kurti)
    }catch(err){
        res.json({
            status:401,
            message:"Please give a number not a string"
        })
    }
    try{
        const people=new People({
            _id:mongoose.Types.ObjectId(),
            name,
            photourl,
            boy,
            girl,
            kurti,
            sharee,
            placeid:req.params.id
        })
        people.save().then(result=>{
            if(result)
            {
                res.json({
                    status:200,
                    message:"Successfully added",
                    data:result
                })
            }
            else{
                res.json({
                    status:401,
                    message:"Some thing goes wrong"
                })
            }
        }).catch(err=>{
            res.json({
                status:401,
                message:"Database error "+err.message
            })
        })
    }catch(err)
    {
        res.json({
            status:401,
            message:"Something goes wrong"
        })
    }
}

//people details for particular places
exports.fetchPeople=(req,res,next)=>{
    try{
        People.find({placeid:req.params.id})
        .exec()
        .then(result=>{
            if(result)
            {
                res.json({
                    status:200,
                    message:"Successfully fetched",
                    data:result
                })
            }else{
                res.json({
                    status:401,
                    message:"Something goes wrong",
                })
            }
        }).catch(err=>{
            res.json({
                status:401,
                message:"Database error "+err.message
            })
        })
    }catch(err)
    {
        res.json({
            status:401,
            message:"Internal server error"
        })
    }
}

//fetching all details of boys
exports.fetchBoys=(req,res)=>{
    try{
        People.find({placeid:req.params.id,boy:{$ne:0}},{girl:0,kurti:0,sharee:0})
        .sort({boy:1})
        .then(result=>{
            if(result)
            {
                res.json({
                    status:200,
                    message:"Successfully fetched the data",
                    data:result
                })
            }else{
                res.json({
                    status:401,
                    message:"Something went wrong"
                })
            }
        }).catch(err=>{
            res.json({
                status:401,
                message:"Database error "+err.message
            })
        })
    }catch(err)
    {
        res.json({
            status:401,
            message:"Internal server error"
        })
    }
}


//fetching girls
exports.fetchGirls=(req,res)=>{
    try{
        People.find({placeid:req.params.id,girl:{$ne:0}},{boy:0,kurti:0,sharee:0})
        .sort({girl:1})
        .then(result=>{
            if(result)
            {
                res.json({
                    status:200,
                    message:"Successfully fetched the data",
                    data:result
                })
            }else{
                res.json({
                    status:401,
                    message:"Something went wrong"
                })
            }
        }).catch(err=>{
            res.json({
                status:401,
                message:"Database error "+err.message
            })
        })
    }catch(err)
    {
        res.json({
            status:401,
            message:"Internal server error"
        })
    }
}

//fetching kurtis from database
exports.fetchKurtis=(req,res)=>{
    try{
        People.find({placeid:req.params.id,kurti:{$ne:0}},{girl:0,boy:0,sharee:0})
        .sort({kurti:1})
        .then(result=>{
            if(result)
            {
                res.json({
                    status:200,
                    message:"Successfully fetched the data",
                    data:result
                })
            }else{
                res.json({
                    status:401,
                    message:"Something went wrong"
                })
            }
        }).catch(err=>{
            res.json({
                status:401,
                message:"Database error "+err.message
            })
        })
    }catch(err)
    {
        res.json({
            status:401,
            message:"Internal server error"
        })
    }
}

//fetching Sharees from database
exports.fetchSharees=(req,res)=>{
    try{
        People.find({placeid:req.params.id,sharee:{$ne:""}},{girl:0,kurti:0,boy:0})
        .sort({sharee:1})
        .then(result=>{
            if(result)
            {
                res.json({
                    status:200,
                    message:"Successfully fetched the data",
                    data:result
                })
            }else{
                res.json({
                    status:401,
                    message:"Something went wrong"
                })
            }
        }).catch(err=>{
            res.json({
                status:401,
                message:"Database error "+err.message
            })
        })
    }catch(err)
    {
        res.json({
            status:401,
            message:"Internal server error"
        })
    }
}

//fetch a particular people
exports.fetchByPeopleId=(req,res)=>{
    try{
        People.findById({_id:req.params.peopleid})
        .then(result=>{
            if(result)
            {
                res.json({
                    status:200,
                    message:"Succefully fetched all the data",
                    data:result
                })
            }else{
                res.json({
                    status:401,
                    message:"Something went wrong"
                })
            }
        }).catch(err=>{
            res.json({
                status:401,
                message:"Database error "+err.message
            })
        })
    }catch(err)
    {
        res.json({
            status:401,
            message:"Internal server error"
        })
    }
}

//deleting all the details by pid(place id)
exports.deleteAllByPid=(req,res)=>{
    try{
        People.deleteMany({placeid:req.params.pid})
        .then(result=>{
            if(result)
            {
                res.json({
                    status:200,
                    message:"Successfully deleted"
                })
            }else{
                res.json({
                    status:401,
                    message:"Something went wrong"
                })
            }
        }).catch(err=>{
            res.json(
                {
                    status:401,
                    message:"Database error "+err.message
                }
            )
        })
    }catch(err)
    {
        res.json({
            status:401,
            message:"Internal server error"
        })
    }
}

//delete only one person by id
exports.deleteOnePerson=(req,res)=>{
    try{
        People.deleteOne({_id:req.params.pid})
        .then(result=>{
            if(result)
            {
                res.json({
                    status:200,
                    message:"Successfully deleted"
                })
            }
            else{
                res.json({
                    status:401,
                    message:"Somethig went wrong"
                })
            }
        }).catch(err=>{
            res.json({
                status:401,
                message:"Database error "+err.message
            })
        })
    }catch(err)
    {
        res.json({
            status:401,
            message:"Internal server error"
        })
    }
}