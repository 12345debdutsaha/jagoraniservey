var admin = require("firebase-admin");

var serviceAccount = require('./servey-service-admin.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://sevey-2a924.firebaseio.com"
});

module.exports=admin