//application set up
const express=require('express')
const router=express.Router()

//requirement of nodemailer
const nodemailer=require('nodemailer')

//nexmo for sending sms
const Nexmo = require('nexmo');

const nexmo = new Nexmo({
  apiKey:process.env.NEXMO_API_KEY,
  apiSecret:process.env.NEXMO_API_SECRET,
});

//router for sending the mail
router.post('/sendmail',async(req,res)=>{
    try{
    let toemail=req.body.toemail
    let transporter = nodemailer.createTransport({
        service:'gmail',
        auth: {
          user:process.env.USER_EMAIL, // generated ethereal user
          pass:process.env.PASSWORD_EMAIL // generated ethereal password
        }
      });
      let info = await transporter.sendMail({
        from:"JAGORANI Debdut gateway", // sender address
        to: toemail, // list of receivers
        subject:req.body.subject, // Subject line
        text: req.body.text, // plain text body
      });
      if(info)
      {
          res.send({message:`Successfully sent the email to ${toemail}`})
      }else{
          res.send({message:"Error occured"})
      }
    }catch(err)
    {
        res.send({message:"Error occured"})
    }

})


//Sending messages from one to another

router.post('/sendsms',(req,res,next)=>{
    nexmo.verify.request({
        number: '918583915287',
        brand: 'Jagorani',
        code_length: '4'
      }, (err, result) => {
        if(err)
        {
            res.send({message:"Error in sending the code"+err.message})
        }
        else{
            res.send({res:result})
        }
      });
})

router.post('/checkid/sms',(req,res,next)=>{

})
module.exports=router