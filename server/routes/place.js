const express=require('express')

const Router=express.Router()

//controller requirement
const PlaceController=require('../controller/placeController')
const authValidator=require('../middlewares/authValidator')
//creation of Places
Router.post('/add',authValidator.verifyJWT,PlaceController.addPlace)

//updation of places
Router.post('/update/:id',authValidator.verifyJWT,PlaceController.updatePlace)

//deleting a particular record from database
Router.delete('/delete/:id',authValidator.verifyJWT,PlaceController.deletePlace)

//deleting all the places from database
Router.post('/delete/all',authValidator.verifyJWT,PlaceController.deletePlaceAll)

//getAll the data from databse
Router.post('/alldata',authValidator.verifyJWT,PlaceController.getAllData)

//get a particular data from database
Router.get('/onedata/:id',authValidator.verifyJWT,PlaceController.getParticularData)

//get all data about places
Router.get('/allData',authValidator.verifyJWT,PlaceController.getAllData)

module.exports=Router
