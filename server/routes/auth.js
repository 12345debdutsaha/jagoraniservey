//basic requirements
const express=require('express')
// express router
const router=express.Router()
//import of admin package
const admin=require('../admin/index')

//middleware requirement
const Validator=require('../middlewares/dataValidator')
const AuthValidator=require('../middlewares/authValidator')
//cryptojs requirement 
const cryptoJs=require('crypto-js')

//signup end point
router.post('/signup',Validator.SignUpCheck,AuthValidator.verifyJWT,(req,res)=>{
    var errors=req.validationErrors()
    try{
    if(!errors && req.valid)
    {
        let email=req.body.email
        let password=req.body.password
        let phonenumber=req.body.phonenumber
        admin.auth().createUser({
            email:email,
            phoneNumber:phonenumber,
            password:password
        }).then((result)=>{
            if(result)
            {
                res.status(200).send({
                    message:"successfully created user",
                    user:result
                })
            }
            else{
                res.send({
                    message:"Internal server error"
                })
            }
        }).catch((err)=>{
            res.send({
                message:err.message
            })
        })
    }else{
        var err={
            message:''
        }
        var err=Validator.ErrorDialog(errors)
        res.status(200).send(err);
    }
}catch(err){
    res.status(500).send({message:'Some unexpected error occured'})
}
})


//Checking of valid jwt
router.post('/check/validJwt',AuthValidator.checkJWT,(req,res)=>{
    if(req.user)
    {
        res.send({
            message:"Valid jwt",
            user:req.user,
            admin:req.admin
        })
    }
    else{
        res.send({
            message:"Some error occured"
        })
    }
})

//checking of valid security key route and producing a jwt that can be delivered to client
router.post('/check/securityKey',AuthValidator.secureKEY,AuthValidator.generateJWT,(req,res)=>{
    if(res.info==="successfull")
    {
        res.send({
            message:'successfull',
            token:req.token
        })
    }else{
        res.send({message:'Please give the right credential'})
    }
})

//giving the site key to the client for recaptcha
router.get('/getrecaptcha',AuthValidator.verifyJWT,(req,res,next)=>{
    if(req.valid)
    {
        let sitekey=process.env.SITE_KEY_CAPTCHA
        res.send({
            message:"successfull",
            captcha:sitekey
        })
    }else{
        res.send({message:"Auth failed"})
    }
})


module.exports=router