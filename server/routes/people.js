const express=require('express')
const router=express.Router()
const peoplecontroller=require('../controller/peoplecontroller')
const authvalidator=require('../middlewares/authValidator')
const People=require('../models/People')
//add details of people in database with place id
router.post('/addpeople/:id',authvalidator.verifyJWT,peoplecontroller.addPeople)


//people details of particular places
router.get('/fetchpeople/:id',authvalidator.verifyJWT,peoplecontroller.fetchPeople)

//fetching the people containing only boys
router.get('/fetchBoys/:id',authvalidator.verifyJWT,peoplecontroller.fetchBoys)

//fetching girls
router.get('/fetchGirls/:id',authvalidator.verifyJWT,peoplecontroller.fetchGirls)

//fetching kurtis
router.get('/fetchKurtis/:id',authvalidator.verifyJWT,peoplecontroller.fetchKurtis)

//fetching Sharees
router.get('/fetchSharees/:id',authvalidator.verifyJWT,peoplecontroller.fetchSharees)

//fetching One's details
router.get('/fetchOnesDetail/:peopleid',authvalidator.verifyJWT,peoplecontroller.fetchByPeopleId)

//deleting all the user records according to their placeid
router.delete('/delete/Places/:pid',authvalidator.verifyJWT,peoplecontroller.deleteAllByPid)

//deleting one person
router.delete('/delete/oneperson/:pid',authvalidator.verifyJWT,peoplecontroller.deleteOnePerson)

//setDone
router.post('/setDone/:id',authvalidator.verifyJWT,async(req,res)=>{
    let id=req.params.id
    let done=req.body.done
    console.log(done)
    await People.updateOne({_id:id},{$set:{done:done}}).then((response)=>{
        res.status(200).send({
            success:true
        })
    }).catch(err=>{
        res.status(401).send({
            success:false
        })
    })
})


module.exports=router