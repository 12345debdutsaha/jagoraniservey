const mongoose=require('mongoose')

const PeopleSchema=mongoose.Schema({
    _id:mongoose.Schema.Types.ObjectId,
    name:{type:String,required:true},
    photourl:{type:String,required:true},
    boy:{type:Number},
    girl:{type:Number},
    kurti:{type:Number},
    sharee:{type:String},
    done:{type:Boolean,default:false},
    placeid:{type:mongoose.Schema.Types.ObjectId,ref:'Place',required:true}
})

module.exports=mongoose.model('People',PeopleSchema)