const mongoose =require('mongoose')

const PlaceSchema=mongoose.Schema({
    _id:mongoose.Schema.Types.ObjectId,
    name:String,
    landmark:String,
    nearLocation:String,
    addressline:String
})
module.exports=mongoose.model('Place',PlaceSchema)