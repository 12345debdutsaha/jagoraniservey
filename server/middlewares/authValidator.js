const admin=require('../admin/index')
const jwt=require('jsonwebtoken')
exports.checkJWT=async(req,res,next)=>{
    const idToken=req.body.jwt
    try{
    await admin.auth().verifyIdToken(idToken).then(decodedToken=>{
        if(decodedToken.email)
        {
            if(decodedToken.email===process.env.admin)
            {
                req.admin=true
            }else{
                req.admin=false
            }
            req.user=decodedToken.email
            next()
        }else{
            res.send({
                message:"Expired"
            })
        }
    }).catch(err=>{
        res.send({
            message:"Expired"
        })
    })
}catch(err)
{
}
}

exports.secureKEY=async(req,res,next)=>{
    const secureKey=req.body.secureKey
    try{
    if(process.env.SECURITY_KEY)
    {
        if(secureKey===process.env.SECURITY_KEY)
        {
            res.info="successfull"
            next()
        }
        else{
            res.info="error"
            next()
        }
    }
}catch(err)
{
    res.send({message:"Internal server error"})
}
}

exports.generateJWT=(req,res,next)=>{
    try{
    let key=req.body.secureKey
    let token=jwt.sign({key},process.env.JWT_KEY,{expiresIn:'1h'})
    req.token=token
    if(token)
    next()
    else
    res.send({message:"Internal server error"})
    }catch(err)
    {
        res.send({message:"Internal server error"})
    }
}

exports.verifyJWT=(req,res,next)=>{
    try{
    let token=req.headers.authorization.split(" ")[1]
    var decodedToken=jwt.verify(token,process.env.JWT_KEY)
    if(decodedToken.key===process.env.SECURITY_KEY)
    {
        req.valid=true
        next()
    }else{
        req.valid=false
        res.send({message:"Your message got tempered"})
    }
    }catch(err)
    {
        req.valid=false
        res.send({message:"Authentication failed "+err.message})
    }
}