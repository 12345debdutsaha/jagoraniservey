//signUp credential checking

exports.SignUpCheck=(req,res,next)=>{
    //email checking
    req.check('email','Invalid email address')
    .isEmail()
    //password validation checking
    req.check('password','Invalid password or Password mismatch')
    .isLength({min:6}).equals(req.body.conpassword)
    //Phonenumber Validation checking
    req.check('phonenumber','Invalid phonenumber')
    .isLength({min:10,max:13})
    next()
}
//Login Credential checking

exports.LoginCheck=(req,res,next)=>{
    //email checking
    req.check('email','Invalid email address')
    .isEmail()
    //password validation checking
    req.check('password','Invalid password')
    .isLength({min:6})
    next()
}
//Data validation error correction

exports.ErrorDialog=(errors)=>{
    var err={message:''}
    errors.map((item,i)=>{
        if(i!=errors.length-1)
        err.message+=item.msg+" and "
        else
        err.message+=item.msg
    })
    return err;
}

