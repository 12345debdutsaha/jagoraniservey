//cluster requirement
const cluster = require('cluster');
const numCPUs = require('os').cpus().length;
//http requirement for creating the server
const http=require('http')


if (cluster.isMaster) {
  console.log(`Master ${process.pid} is running`);

  // Fork workers.
  for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  cluster.on('exit', (worker, code, signal) => {
    console.log(`worker ${worker.process.pid} died`);
  });
} else {
  // Workers can share any TCP connection
  // In this case it is an HTTP server
  //basic reuires for running the server with middlewares
const express=require('express')
const app=express()
const path=require('path')
const bodyParser=require('body-parser')
const expressValidator=require('express-validator')
const cors=require('cors')
//mongoose requirement
const mongoose=require('mongoose')


//<------------development enviuornment variable setup///---------->
// const bin=require('./bin/dev');

//<-------database connection setup start
mongoose.connect('mongodb+srv://'
+process.env.MONGO_USERNAME+':'
+process.env.MONGO_PW+
'@jagoraniservey-4rdyg.mongodb.net/test?retryWrites=true&w=majority',
{useNewUrlParser:true},(err)=>{
  if(err)
  {
    console.log("mongo error")
  }else{
    console.log("Mongo is connected")
  }
})
//database connection setup ends here------------->

//<-----------Routes requirement start here
const auth=require('./server/routes/auth')
const network=require('./server/routes/networking')
const place=require('./server/routes/place')
const people=require('./server/routes/people')
//Rotes requirement ends here------------>


//port deffination
const PORT=process.env.PORT || 5000

//path for react production build file
const pathreact=__dirname+"/build"+'/index.html'


//http create server
const server=http.createServer(app);

//<----------------cross origin request handler starts here
app.use(cors())
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next()
});
//cross origin request handler ends here------------->

//body parser for encoding the value in utf-8 and provides to the req object
// parse application/x-www-form-urlencoded 
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

//express-validator middleware it is strictly after body parser
//validator for form validation in server side
app.use(expressValidator())

//for react files middleware
app.use(express.static(path.resolve(__dirname+'/build')))

//<--------------middlewares for router starts here
app.use('/api/auth',auth)
app.use('/api/network',network)
app.use('/place',place)
app.use('/people',people)
//middlewares for router ends here----------->


app.get('*', (req, res) => {
  res.sendFile(pathreact);
});

//server listening to the port
server.listen(PORT,()=>{
    console.log("server is running at",PORT)
})
}